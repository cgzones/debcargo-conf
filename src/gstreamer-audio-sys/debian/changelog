rust-gstreamer-audio-sys (0.22.0-2) unstable; urgency=medium

  * Package gstreamer-audio-sys 0.22.0 from crates.io using debcargo 2.6.1
  * Stop marking v1_24 feature as broken

 -- Matthias Geiger <werdahias@riseup.net>  Sun, 05 May 2024 13:02:59 +0200

rust-gstreamer-audio-sys (0.22.0-1) experimental; urgency=medium

  * Package gstreamer-audio-sys 0.22.0 from crates.io using debcargo 2.6.1
  * Stop regenerating code since upstream uses git snapshots of gir
    files
  * Updated copyright years and Source: url in d/copyright

 -- Matthias Geiger <werdahias@riseup.net>  Sat, 24 Feb 2024 02:31:50 +0100

rust-gstreamer-audio-sys (0.21.0-4) unstable; urgency=medium

  * Team upload.
  * Package gstreamer-audio-sys 0.21.0 from crates.io using debcargo 2.6.0
  * Add patch to ignore failures for autopkgtests that require an
    unreleased gstreamer version

 -- Jeremy Bícha <jbicha@ubuntu.com>  Fri, 29 Sep 2023 10:52:02 -0400

rust-gstreamer-audio-sys (0.21.0-3) unstable; urgency=medium

  * Team upload
  * Package gstreamer-audio-sys 0.21.0 from crates.io using debcargo 2.6.0
  * Release to unstable

 -- Jeremy Bícha <jbicha@ubuntu.com>  Thu, 28 Sep 2023 15:34:23 -0400

rust-gstreamer-audio-sys (0.21.0-2) experimental; urgency=medium

  * Team upload
  * Drop patch downgrading required Rust version
  * Don't re-generate source code: requires unreleased gstreamer

 -- Jeremy Bícha <jbicha@ubuntu.com>  Wed, 27 Sep 2023 15:08:48 -0400

rust-gstreamer-audio-sys (0.21.0-1) experimental; urgency=medium

  * Package gstreamer-audio-sys 0.21.0 from crates.io using debcargo 2.6.0
  * Included patch for MSRV downgrade
  * Regenerate source code with debian tools before build
  * Mark v1_24 feature as flaky for now

 -- Matthias Geiger <werdahias@riseup.net>  Fri, 08 Sep 2023 20:43:11 +0200

rust-gstreamer-audio-sys (0.20.0-1) unstable; urgency=medium

  * Package gstreamer-audio-sys 0.20.0 from crates.io using debcargo 2.6.0
  * Updated my mail address
  * Rebased patch

 -- Matthias Geiger <werdahias@riseup.net>  Wed, 02 Aug 2023 00:11:11 +0200

rust-gstreamer-audio-sys (0.19.4-3) unstable; urgency=medium

  * Package gstreamer-audio-sys 0.19.4 from crates.io using debcargo 2.6.0

 -- Matthias Geiger <matthias.geiger1024@tutanota.de>  Sun, 25 Jun 2023 20:50:12 +0200

rust-gstreamer-audio-sys (0.19.4-2) experimental; urgency=medium

  * Package gstreamer-audio-sys 0.19.4 from crates.io using debcargo 2.6.0
  * Include patch to pass autopkgtest on s390x

 -- Matthias Geiger <matthias.geiger1024@tutanota.de>  Sun, 28 May 2023 14:36:46 +0200

rust-gstreamer-audio-sys (0.19.4-1) experimental; urgency=medium

  * Package gstreamer-audio-sys 0.19.4 from crates.io using debcargo 2.6.0
  * Added myself to uploaders, dropped slomo@debian.org
  * Added build-depends in debcargo.toml
  * Dropped obsolete patches

 -- Matthias Geiger <matthias.geiger1024@tutanota.de>  Sat, 20 May 2023 13:54:00 +0200

rust-gstreamer-audio-sys (0.17.0-2) unstable; urgency=medium

  * Team upload.
  * debian/patches: Update to system-deps 6

 -- Sebastian Ramacher <sramacher@debian.org>  Wed, 05 Oct 2022 10:05:59 +0200

rust-gstreamer-audio-sys (0.17.0-1) unstable; urgency=medium

  * Team upload.
  * Package gstreamer-audio-sys 0.17.0 from crates.io using debcargo 2.5.0 (Closes: 1002122)
  * Mark tests as broken, they have never passed.

 -- Peter Michael Green <plugwash@debian.org>  Thu, 30 Dec 2021 11:52:05 +0000

rust-gstreamer-audio-sys (0.8.0-2) unstable; urgency=medium

  * Team upload.
  * Package gstreamer-audio-sys 0.8.0 from crates.io using debcargo 2.4.2

 -- Sylvestre Ledru <sylvestre@debian.org>  Mon, 20 Apr 2020 14:08:16 +0200

rust-gstreamer-audio-sys (0.8.0-1) unstable; urgency=medium

  * Package gstreamer-audio-sys 0.8.0 from crates.io using debcargo 2.4.0

 -- Sebastian Dröge <slomo@debian.org>  Tue, 19 Nov 2019 11:23:12 +0200
