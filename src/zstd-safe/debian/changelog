rust-zstd-safe (7.2.0-1) unstable; urgency=medium

  * Package zstd-safe 7.2.0 from crates.io using debcargo 2.6.1
  * Drop temporary TargetCBlockSize patch

 -- Blair Noctis <n@sail.ng>  Sat, 20 Jul 2024 19:30:26 +0000

rust-zstd-safe (7.1.0-2) unstable; urgency=medium

  * Team upload.
  * Temporarily patch out target block size feature only available with libzstd 1.5.6

 -- Fabian Grünbichler <debian@fabian.gruenbichler.email>  Mon, 17 Jun 2024 21:07:35 +0200

rust-zstd-safe (7.1.0-1) unstable; urgency=medium

  * Package zstd-safe 7.1.0 from crates.io using debcargo 2.6.1
  * Add myself to Uploaders

 -- Blair Noctis <n@sail.ng>  Sat, 15 Jun 2024 14:37:11 +0000

rust-zstd-safe (7.0.0-1) experimental; urgency=medium

  * Team upload.
  * Package zstd-safe 7.0.0 from crates.io using debcargo 2.6.1
  * Drop empty-bindgen-and-pkg-config-features.diff
  * Update remove-unavailable-features.diff with updated features
  * Improve package description

 -- Blair Noctis <n@sail.ng>  Fri, 01 Dec 2023 02:17:02 +0000

rust-zstd-safe (6.0.2-1) unstable; urgency=medium

  * Team upload.
  * Package zstd-safe 6.0.2+zstd.1.5.2 from crates.io using debcargo 2.6.0
  * Merge fix-bindgen.diff and fix-pkg-config.diff, same origin

 -- Blair Noctis <n@sail.ng>  Fri, 06 Jan 2023 16:56:09 +0100

rust-zstd-safe (5.0.2-2) unstable; urgency=medium

  * Team upload.
  * Package zstd-safe 5.0.2+zstd.1.5.2 from crates.io using debcargo 2.6.0
  * Re-enable std feature (Closes: #1025101)

 -- Peter Michael Green <plugwash@debian.org>  Thu, 01 Dec 2022 01:35:54 +0000

rust-zstd-safe (5.0.2-1) unstable; urgency=medium

  * Package zstd-safe 5.0.2+zstd.1.5.2 from crates.io using debcargo 2.5.0
  * Remove copyright notice for lib.rs as upstream marked the issue addressed
  * Remove unavailable zstd features
  * Make bindgen and pkg-config empty features as zstd-sys mandates them
  * Fix test requiring zdict_builder feature
  * Remove patch relaxing zstd-sys as upstream catched up

  [ Sylvestre Ledru ]
  * Package zstd-safe 2.0.5+zstd.1.4.5 from crates.io using debcargo 2.4.3

  [ Ximin Luo ]
  * Team upload.
  * Package zstd-safe 2.0.3+zstd.1.4.4 from crates.io using debcargo 2.4.2

  [ Fabian Grünbichler ]
  * Package zstd-safe 2.0.3+zstd.1.4.4 from crates.io using debcargo 2.4.1-alpha.0

 -- Blair Noctis <n@sail.ng>  Mon, 19 Sep 2022 21:52:54 +0200
