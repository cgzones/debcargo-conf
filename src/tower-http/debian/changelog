rust-tower-http (0.4.4-4) unstable; urgency=medium

  * Team upload.
  * Update dep on zstd to packaged 0.13, brotli to 6
  * Drop relaxing bytes 1 -> 0.2.9

 -- Blair Noctis <n@sail.ng>  Mon, 17 Jun 2024 21:19:07 +0200

rust-tower-http (0.4.4-3) unstable; urgency=medium

  * Team upload.
  * Package tower-http 0.4.4 from crates.io using debcargo 2.6.1
  * Skip tests that require AtomicU64 on architectures where it is unavailable.

 -- Peter Michael Green <plugwash@debian.org>  Wed, 14 Feb 2024 03:33:25 +0000

rust-tower-http (0.4.4-2) unstable; urgency=medium

  * Team upload.
  * Package tower-http 0.4.4 from crates.io using debcargo 2.6.1
  * Stop patching mime dependency, it turns out the newer version is actually
    needed in some feature configurations. (Closes: #1063784)
  * Bump some deps to allow testing with -Z minimal-versions.
  * Deal with tests that require test data that is not included in the crates.io
    release. Some tests are modified to use different test data, others are
    disabled.
  * Fix test feature requirements.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 13 Feb 2024 11:07:46 +0000

rust-tower-http (0.4.4-1) unstable; urgency=medium

  * Team upload.
  * Package tower-http 0.4.4 from crates.io using debcargo 2.6.1

  [ Jelmer Vernooĳ ]
  * Package tower-http 0.4.4 from crates.io using debcargo 2.6.0
  * Closes: #1053599

 -- Reinhard Tartler <siretart@tauware.de>  Sat, 27 Jan 2024 15:00:14 -0500
