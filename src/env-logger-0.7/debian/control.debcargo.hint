Source: rust-env-logger-0.7
Section: rust
Priority: optional
Build-Depends: debhelper (>= 12),
 dh-cargo (>= 25),
 cargo:native <!nocheck>,
 rustc:native <!nocheck>,
 libstd-rust-dev <!nocheck>,
 librust-atty-0.2+default-dev (>= 0.2.5-~~) <!nocheck>,
 librust-humantime-2+default-dev <!nocheck> | librust-humantime-1+default-dev (>= 1.3-~~) <!nocheck>,
 librust-log-0.4+default-dev (>= 0.4.8-~~) <!nocheck>,
 librust-log-0.4+std-dev (>= 0.4.8-~~) <!nocheck>,
 librust-regex-1+default-dev (>= 1.0.3-~~) <!nocheck>,
 librust-termcolor-1+default-dev (>= 1.0.2-~~) <!nocheck>
Maintainer: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Uploaders:
 Wolfgang Silbermayr <wolfgang@silbermayr.at>
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/rust-team/debcargo-conf.git [src/env-logger-0.7]
Vcs-Browser: https://salsa.debian.org/rust-team/debcargo-conf/tree/master/src/env-logger-0.7
X-Cargo-Crate: env_logger
Rules-Requires-Root: no

Package: librust-env-logger-0.7-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-log-0.4+default-dev (>= 0.4.8-~~),
 librust-log-0.4+std-dev (>= 0.4.8-~~)
Recommends:
 librust-env-logger-0.7+default-dev (= ${binary:Version})
Suggests:
 librust-env-logger-0.7+atty-dev (= ${binary:Version}),
 librust-env-logger-0.7+humantime-dev (= ${binary:Version}),
 librust-env-logger-0.7+regex-dev (= ${binary:Version}),
 librust-env-logger-0.7+termcolor-dev (= ${binary:Version})
Provides:
 librust-env-logger-dev (= ${binary:Version}),
 librust-env-logger-0-dev (= ${binary:Version}),
 librust-env-logger-0.7.1-dev (= ${binary:Version})
Replaces: librust-env-logger-0.7.1-dev
Breaks: librust-env-logger-0.7.1-dev
Description: Log implementation configured via environment variable - Rust source code
 Source code for Debianized Rust crate "env_logger"

Package: librust-env-logger-0.7+atty-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-env-logger-0.7-dev (= ${binary:Version}),
 librust-atty-0.2+default-dev (>= 0.2.5-~~)
Provides:
 librust-env-logger+atty-dev (= ${binary:Version}),
 librust-env-logger-0+atty-dev (= ${binary:Version}),
 librust-env-logger-0.7.1+atty-dev (= ${binary:Version})
Description: Log implementation configured via environment variable - feature "atty"
 This metapackage enables feature "atty" for the Rust env_logger crate, by
 pulling in any additional dependencies needed by that feature.

Package: librust-env-logger-0.7+default-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-env-logger-0.7-dev (= ${binary:Version}),
 librust-env-logger-0.7+termcolor-dev (= ${binary:Version}),
 librust-env-logger-0.7+atty-dev (= ${binary:Version}),
 librust-env-logger-0.7+humantime-dev (= ${binary:Version}),
 librust-env-logger-0.7+regex-dev (= ${binary:Version})
Provides:
 librust-env-logger+default-dev (= ${binary:Version}),
 librust-env-logger-0+default-dev (= ${binary:Version}),
 librust-env-logger-0.7.1+default-dev (= ${binary:Version})
Description: Log implementation configured via environment variable - feature "default"
 This metapackage enables feature "default" for the Rust env_logger crate, by
 pulling in any additional dependencies needed by that feature.

Package: librust-env-logger-0.7+humantime-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-env-logger-0.7-dev (= ${binary:Version}),
 librust-humantime-2+default-dev | librust-humantime-1+default-dev (>= 1.3-~~)
Provides:
 librust-env-logger+humantime-dev (= ${binary:Version}),
 librust-env-logger-0+humantime-dev (= ${binary:Version}),
 librust-env-logger-0.7.1+humantime-dev (= ${binary:Version})
Description: Log implementation configured via environment variable - feature "humantime"
 This metapackage enables feature "humantime" for the Rust env_logger crate, by
 pulling in any additional dependencies needed by that feature.

Package: librust-env-logger-0.7+regex-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-env-logger-0.7-dev (= ${binary:Version}),
 librust-regex-1+default-dev (>= 1.0.3-~~)
Provides:
 librust-env-logger+regex-dev (= ${binary:Version}),
 librust-env-logger-0+regex-dev (= ${binary:Version}),
 librust-env-logger-0.7.1+regex-dev (= ${binary:Version})
Description: Log implementation configured via environment variable - feature "regex"
 This metapackage enables feature "regex" for the Rust env_logger crate, by
 pulling in any additional dependencies needed by that feature.

Package: librust-env-logger-0.7+termcolor-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-env-logger-0.7-dev (= ${binary:Version}),
 librust-termcolor-1+default-dev (>= 1.0.2-~~)
Provides:
 librust-env-logger+termcolor-dev (= ${binary:Version}),
 librust-env-logger-0+termcolor-dev (= ${binary:Version}),
 librust-env-logger-0.7.1+termcolor-dev (= ${binary:Version})
Description: Log implementation configured via environment variable - feature "termcolor"
 This metapackage enables feature "termcolor" for the Rust env_logger crate, by
 pulling in any additional dependencies needed by that feature.
